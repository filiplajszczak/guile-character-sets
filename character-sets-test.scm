(use-modules (srfi srfi-64)
      	     (kata-driver))

(test-runner-factory
 (lambda () (test-runner-kata)))

(define (password-valid? password)
  (if (string-null? password)
      #f
      (char-set<= (string->char-set password)
                  (char-set-union char-set:letter char-set:digit))))

(test-begin "test-suite")

(test-equal "empty password"
  #f
  (password-valid? ""))

(test-equal "one letter password"
  #t
  (password-valid? "a"))

(test-equal "letters and digits password"
  #t
  (password-valid? "ab12"))

(test-end "test-suite")
