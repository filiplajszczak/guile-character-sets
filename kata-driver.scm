(define-module (kata-driver)
  #:export (test-runner-kata))

(use-modules (srfi srfi-64)
             (ice-9 pretty-print)
             (srfi srfi-26))

(define* (test-display field value  #:optional (port (current-output-port))
                       #:key pretty?)
  "Display 'FIELD: VALUE\n' on PORT."
  (if pretty?
      (begin
        (format port "~A:~%" field)
        (pretty-print value port #:per-line-prefix "+ "))
      (format port "~A: ~S~%" field value)))

(define* (result->string symbol)
  "Return SYMBOL as an upper case string.  Use colors when COLORIZE is #t."
  (let ((result (string-upcase (symbol->string symbol))))
    (string-append (case symbol
                     ((pass)       "[0;32m")  ;green
                     ((xfail)      "[1;32m")  ;light green
                     ((skip)       "[1;34m")  ;blue
                     ((fail xpass) "[0;31m")  ;red
                     ((error)      "[0;35m")) ;magenta
                   result
                   "[m")))

(define* (test-runner-kata)

  (define (test-on-test-end-kata runner)
    (let* ((results (test-result-alist runner))
           (result? (cut assq <> results))
           (result  (cut assq-ref results <>)))
      (if (equal? 'fail (result 'result-kind))
	  (begin
	    (newline)
	    (format #t "~a ~A~%"
		    (result->string (result 'result-kind))
		    (result 'test-name))
	    (when (result? 'expected-value)
              (test-display "expected-value" (result 'expected-value)))
	    (when (result? 'expected-error)
              (test-display "expected-error" (result 'expected-error) #:pretty? #t))
	    (when (result? 'actual-value)
              (test-display "actual-value" (result 'actual-value)))
	    (when (result? 'actual-error)
	      (test-display "actual-error" (result 'actual-error) #:pretty? #t))
	    (newline))
	  (begin
	    (format #t "~a ~A~%"
		    (result->string (result 'result-kind))
		    (result 'test-name))))))

  (let ((runner (test-runner-null)))
    (test-runner-on-test-end! runner test-on-test-end-kata)
    runner))
