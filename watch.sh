#!/usr/bin/env bash

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

run () {
    local test_file_name="$1"
    guild compile -W2 *.scm -L .
    guile --no-auto-compile -L . $1
}

main () {
    local dir_to_watch=$(basename "$PWD")
    local project_name="${dir_to_watch#*-}"
    local test_file_name="${project_name}-test.scm"

    run $test_file_name
    
    while true;
    do
    	watch -t -g -n 0.1 'ls -lR --full-time .' > /dev/null
    	clear
	      run $test_file_name
    done
}

main
